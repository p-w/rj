import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import layoutsRegistry from '../../../jekyll/_layouts/registry'
import site from '../../site'
import pizdec from '../pizdec'
import pages from '../../pages'
import {getLocalizedSeoString} from '../../localeUtils'

@connect(
  state => state,
  dispatch => bindActionCreators({}, dispatch)
)
export default class extends Component {

  render() {
    const {page} = this.props.route

    const LayoutComponent = layoutsRegistry[page.layout]

    pizdec.currentRenderingPage = page

    if (!LayoutComponent) {
      return (
        <div>
          Layout {layout} not found
        </div>
      )
    }

    return (
      <LayoutComponent page={page} site={site}/>
    )
  }
}
