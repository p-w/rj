import React from 'react'

export class AboutView extends React.Component {
  render() {
    return (
      <div className="container text-center">
        <h1>Doh! 404!</h1>
        <p>These are <em>not</em> the droids you are looking for!</p>
        <hr />
      </div>
    )
  }
}

export default AboutView
