import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import layoutsRegistry from '../../../jekyll/_layouts/registry'
import pizdec from '../pizdec'
import site from '../../site'

@connect(
  state => state,
  dispatch => bindActionCreators({}, dispatch)
)
export default class extends Component {
  render() {
    const
      layout = 'SelectLanguage',
      LayoutComponent = layoutsRegistry[layout]

    pizdec.currentRenderingPage = {
      title: 'Select language',
      description: 'Please select your preferred language'
    }

    if (!LayoutComponent) {
      return (
        <div>
          Layout {layout} not found
        </div>
      )
    }

    return (
      <LayoutComponent site={{...site}} page={{}}/>
    )
  }
}
