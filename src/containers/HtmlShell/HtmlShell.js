import React, {Component, PropTypes} from 'react'
import ReactDOM from 'react-dom/server'
import serialize from 'serialize-javascript'
import pizdec from '../pizdec'
import pages from '../../pages'


import {getLocalizedSeoString} from '../../localeUtils'


/**
 * Wrapper component containing HTML metadata and boilerplate tags.
 * Used in server-side code only to wrap the string output of the
 * rendered route component.
 *
 * The only thing this component doesn't (and can't) include is the
 * HTML doctype declaration, which is added to the rendered output
 * by the server.js file.
 */
export default class Html extends Component {
  static propTypes = {
    assets: PropTypes.object,
    component: PropTypes.node,
    store: PropTypes.object
  };

  renderScripts() {
    if (process.env.RENDER === '1') return

    const {store, assets} = this.props

    return (
      <div>
        <script dangerouslySetInnerHTML={{__html: `window.__data=${serialize(store.getState())}`}} charSet="UTF-8"/>
        {Object.keys(assets.javascript).map((jsAsset, key) =>
          <script src={assets.javascript[jsAsset]} key={key} charSet="UTF-8"/>
        )}
      </div>
    )
  }

  renderAssets() {
    if (process.env.RENDER === '1') {
      return (
        <link href="/index.css" media="screen, projection" rel="stylesheet"
              type="text/css" charset="UTF-8"/>
      )
    } else {
      const {assets} = this.props

      return Object.keys(assets.styles).map((style, key) =>
        <link href={assets.styles[style]} key={key} media="screen, projection"
              rel="stylesheet" type="text/css" charSet="UTF-8"/>
      )
    }
  }


  render() {

    const
      {component} = this.props,
      content = component ? ReactDOM.renderToString(component) : '',
      { currentRenderingPage:page } = pizdec,
      {title, description} = page

    return (
      <html lang="en-us">
      <head>
        <title>{title}</title>
        <link href="//fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic" rel="stylesheet"
              type="text/css"/>
        <meta name="description" content={description}/>
        <meta name="HandheldFriendly" content="True"/>
        <meta name="MobileOptimized" content="320"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta httpEquiv="cleartype" content="on"/>
        {this.renderAssets()}

      </head>
      <body>
      <div id="content" dangerouslySetInnerHTML={{__html: content}}/>
      {this.renderScripts()}
      </body>
      </html>
    )
  }
}
