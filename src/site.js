import pages from './pages'
import dataFiles from './jekyllWatch/dataFiles'
import {layoutsMap} from '../jekyll/_config'
import R from 'ramda'
import {getTagValue} from './utils'

function createTags() {
  const tags = Object.keys(pages).reduce((tags, key)=> {
    const
      pageRecord = pages[key],
      page = pageRecord[Object.keys(pageRecord)[0]]

    if (page.layout === 'Index') {
      return tags
    }

    const {urlTags} = page,
      urlTagsCombinations = urlTags.reduce((acc, tag)=> {
        acc.result.push([...acc.prevTags, tag].join('-'))
        return {...acc, prevTags: [...acc.prevTags, tag]}
      }, {result: [...urlTags], prevTags: []}),
      allTags = [...urlTagsCombinations.result, ...(page.tags || [])] || []
    if (allTags.length) {
      allTags.forEach(tag=> {
        if (!tags[tag]) {
          tags[tag] = []
        }

        if (tags[tag].indexOf(pageRecord) === -1) {
          tags[tag].push(pageRecord)
        }
      })
    }
    return tags
  }, {})

  return tags
}

export default {
  data: {
    ...dataFiles
  },
  tags: createTags(),
  pages: pages
}
