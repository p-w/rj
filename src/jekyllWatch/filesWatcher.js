import watch from 'watch'
import { extname, join, basename} from 'path'
import { writeFileSync } from 'fs'
import {parse as parseFrontMatter} from 'markdown-with-front-matter-loader'
import fs from 'fs'
import imagesExtensions from '../imagesExtensions'

const debug = require('debug')('app:jekyll:fileWatcher'),
  spawn = require('child_process').spawn,
  rowsSplitter = ',\n'

let files = {},
  timeoutToken,
  serverProcess

function writeJekyllFiles() {
  const
    jsonMapper = filePath => {
      return `'${filePath.replace('jekyll/_data/', '').replace('.json', '')}': require('../../${filePath}')`
    },
    mdMapper = filePath => {
      const
        data = getFileData(filePath),
        urlTags = getUrlTags(filePath),
        key = urlTags.join('-')

      if (filePath.indexOf('index_gen.md') !== -1) {
        return `'${key}-gen': ${JSON.stringify(data)}`
      }
      return `'${key}': require('../../${filePath}')`
    },
    jsonRows = Object.keys(files)
      .filter(filePath => extname(filePath) === '.json')
      .map(jsonMapper),
    mdRows = Object.keys(files)
      .filter(filePath => extname(filePath) === '.md')
      .map(mdMapper),
    imagesMap = Object.keys(files)
      .filter(filePath => imagesExtensions.indexOf(extname(filePath)) !== -1)
      .reduce((imagesMap, filePath) => {
        const
          urlTags = getUrlTags(filePath),
          key = urlTags.join('-')

        if(!imagesMap[key]){
          imagesMap[key] = []
        }
        imagesMap[key].push(basename(filePath))
        return imagesMap
      }, {}),
    jsonFilesResult =
      `export default {
${jsonRows.join(rowsSplitter)}
}`,
    mdFilesResult =
      `export default {
${mdRows.join(rowsSplitter)}
}`,
    imagesResult = `export default ${JSON.stringify(imagesMap)}`,
    dataFileName = join(__dirname, 'dataFiles.js'),
    pagesFileName = join(__dirname, 'pagesFiles.js'),
    imagesFileName = join(__dirname, 'imagesFiles.js')

  writeFileSync(dataFileName, jsonFilesResult)
  writeFileSync(pagesFileName, mdFilesResult)
  writeFileSync(imagesFileName, imagesResult)
  console.log(`${dataFileName},${pagesFileName},${imagesFileName} updated`)
}

function restartServer() {
  if (serverProcess) {
    serverProcess.kill('SIGHUP')
  }
  serverProcess = spawn('node', ['./bin/start'])

  serverProcess.stdout.on('data', function (data) {
    console.log('server output: ' + data);
  });

  serverProcess.stderr.on('data', function (data) {
    console.log('server output: : ' + data);
  })
}

function getUrlTags(filePath) {
  const pathParts = filePath.split('/')
  return pathParts.slice(1, pathParts.length - 1)
}
function getFileData(filePath) {
  const contentString = fs.readFileSync(filePath)
  return parseFrontMatter(contentString)
}

function updateJekyllsFilesList() {
  if (timeoutToken) {
    clearTimeout(timeoutToken)
  }

  timeoutToken = setTimeout(()=> {
    writeJekyllFiles()
  }, 100)
}

function handleCreated(folder, stat) {
  if (!extname(folder)) {
    watch.createMonitor(folder, monitorCallback)
    debug('new folder', folder)
  } else {
    debug('new file', folder)
    files[folder] = stat
    updateJekyllsFilesList()
  }
}

function monitorCallback(monitor) {
  files = {...files, ... monitor.files}
  updateJekyllsFilesList()

  monitor.on('created', handleCreated)

  monitor.on('removed', (folder)=> {
    debug('file removed', folder)
    delete files[folder]
    updateJekyllsFilesList()
  })
}

function watchJekyll() {
  watch.createMonitor(`jekyll`, monitorCallback)
}

function watchWebpackAssetsAndRoutes() {
  watch.createMonitor('node_modules/universal-redux/', function monitorCallback(monitor) {
    monitor.on('changed', (file)=> {
      console.log('changed', file)
      if (file.indexOf('webpack-assets.json') !== -1) {
        restartServer()
      }
    })
  })

  watch.createMonitor('src/', function monitorCallback(monitor) {
    monitor.on('changed', (file)=> {
      console.log('changed', file)
      restartServer()
    })
  })
}


export function start() {
  watchJekyll()
  watchWebpackAssetsAndRoutes()
  restartServer()
}
