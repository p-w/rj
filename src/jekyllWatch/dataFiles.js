export default {
'items': require('../../jekyll/_data/items.json'),
'items_loc_seo_gen': require('../../jekyll/_data/items_loc_seo_gen.json'),
'items_loc_url_gen': require('../../jekyll/_data/items_loc_url_gen.json'),
'items_tags_loc_url': require('../../jekyll/_data/items_tags_loc_url.json'),
'items_tags_loc_url_gen': require('../../jekyll/_data/items_tags_loc_url_gen.json'),
'loc_items': require('../../jekyll/_data/loc_items.json'),
'loc_menu': require('../../jekyll/_data/loc_menu.json'),
'loc_url': require('../../jekyll/_data/loc_url.json')
}