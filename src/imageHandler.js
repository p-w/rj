import {basename, extname, join} from 'path'
import imagesExtensions from './imagesExtensions'
import {get as getPagesByUrl} from './pagesByUrl'



export default function (req, res, next){
  const
    path = decodeURI(req.path),
    extName = extname(path),
    fileName = basename(path)

  if(imagesExtensions.indexOf(extName)!==-1){
    const
      pagesByUrl = getPagesByUrl(),
      folderPath = path.replace(fileName, ''),
      page = pagesByUrl[folderPath],
      imagePath = page.images[fileName]

    res.sendFile(join(__dirname, '../jekyll', imagePath))
  }else{
    next()
  }
}