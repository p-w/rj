import u from 'updeep'
import memo from 'memoizee'
import dataFiles from './jekyllWatch/dataFiles'

export function localize(localizationFile, type, locale, key) {
  if (key === undefined) return

  const record = localizationFile[key]

  if (!record || (type ? (!record[type] || !record[type][locale]) : !record[locale])) {
    return
  }

  return type ? record[type][locale] : record[locale]
}

export const getLocalizedString = memo(function (fileName, type, locale, key, fallbackKey) {
  const localizationData = getLocalizationData(fileName)

  if (!localizationData) {
    const error = `WARNING: _data/${fileName}.json is not exists, can't get localization`

    console.log(error)
    return error
  }

  return (
    localize(localizationData, type, locale, key) ||
    localize(localizationData, type, locale, fallbackKey) ||
    missing(fallbackKey || key, type, locale, fileName)
  )
})

const getLocalizationData = memo(function (fileName) {
  const
    getFileName = `${fileName}_gen`,
    locFileNames = Object.keys(dataFiles),
    loc_seo_genFiles = locFileNames.filter(currentFileName =>
      currentFileName.indexOf(getFileName) === currentFileName.length - getFileName.length
    ),
    loc_seoFiles = locFileNames.filter(currentFileName =>
      currentFileName.indexOf(fileName) === currentFileName.length - fileName.length
    ),
    genFilesDataParts = loc_seo_genFiles.map(fileName=>dataFiles[fileName]),
    filesDataParts = loc_seoFiles.map(fileName=>dataFiles[fileName]),
    allUpdates = [...genFilesDataParts, ...filesDataParts],
    localizationData = allUpdates.reduce((localizationData, update)=>{
      return u(update, localizationData)
    },{})

  return localizationData
})

export const getLocalizedSeoString = memo(function (type, locale, urlTags) {
  const
    locData = getLocalizationData('loc_seo'),
    localization = localize(locData, type, locale, urlTags)

  if (!localization) {
    missing(urlTags, type, locale, 'seo')
  }

  return localization
})

export const getLocalizedUrlString = memo(function (locale, urlTag) {
  const
    fileName = 'loc_url',
    locData = getLocalizationData(fileName),
    localization = localize(locData, undefined, locale, urlTag)

  if (!localization) {
    missing(urlTag, undefined, locale, fileName)
  }

  return localization
})

function missing(key, type, locale, fileName) {
  const error = `${key}(${locale}) ${type ? `of type ${type} ` : ''}not found in ${fileName}`
  console.log('missing translation', error)
  return error
}