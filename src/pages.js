import pagesFiles from './jekyllWatch/pagesFiles'
import imagesFiles from './jekyllWatch/imagesFiles'
import u from 'updeep'
import {getLocalizedSeoString, getLocalizedUrlString} from './localeUtils'
import {locales} from '../jekyll/_config'
import {layoutsMap} from '../jekyll/_config'
import {getTagValue} from './utils'
import {set as setPagesByUrl} from './pagesByUrl'

const
  genPostfix = '-gen',
  pages = (function make() {
    const pages = Object.keys(pagesFiles).reduce((pages, key)=> {
      if (key.endsWith(genPostfix)) {
        const
          originalKey = key.substring(0, key.lastIndexOf(genPostfix)),
          originalPage = pagesFiles[originalKey] || {}

        pages[originalKey] = {...u(pagesFiles[key], originalPage)}
      } else {
        pages[key] = pagesFiles[key]
      }

      return pages
    }, {})

    const result = Object.keys(pages).reduce((result, key)=> {
      const
        page = pages[key]

      result[key] = locales.reduce((result, locale)=> {
        const
          title = getLocalizedSeoString('title', locale, key),
          description = getLocalizedSeoString('description', locale, key),
          urlTags = key.split('-'),
          urlParts = [locale].concat(urlTags.map(tag=> {
            return getLocalizedUrlString(locale, tag) || tag
          })),
          url = `/${urlParts.join('/')}/`.replace('//', '/'),
          urlTagsMap = urlTags.reduce((map, tagPair)=> {
            const
              key = tagPair.substring(0, tagPair.indexOf('_')),
              value = tagPair.substring(key.length + 1)

            map[key] = value

            return map
          }, {}),
          imagesRecord = imagesFiles[key],
          images = imagesRecord && imagesRecord.reduce((images, image)=> {
              if (image.indexOf('_gen') !== -1) {
                const withOutGen = image.replace('_gen', '')

                if (imagesRecord.indexOf(withOutGen) !== -1)
                {
                  return images
                }
              }

              function getImageKey(sourceKey) {
                if (sourceKey === 'index.jpg' || sourceKey === 'index_gen.jpg') {
                  return `${urlParts[urlParts.length - 1]}.jpg`
                }
                return sourceKey
              }

              images[getImageKey(image)] = `${urlTags.join('/')}/${image}`
              return images
            }, {})

        result[locale] = {
          ...page,
          id: key,
          locale,
          title,
          description,
          urlTags,
          urlTagsMap,
          url,
          images,
          layout: page.layout || layoutsMap[getTagValue('type', urlTags)]
        }

        return result
      }, {})

      return result
    }, {})

    return result
  })()

export default pages

setPagesByUrl(Object.keys(pages).reduce((pagesByUrl, key)=> {
  const pageRecord = pages[key]
  Object.keys(pageRecord).forEach(locale=> {
    const page = pageRecord[locale]
    pagesByUrl[page.url] = page
  })
  return pagesByUrl
}, {}))