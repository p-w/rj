
export function getTagValue(type, tags) {
  const idPrefix = `${type}_`,
    tag = tags.filter(tag=>tag.startsWith(idPrefix))[0]

  return tag && tag.replace(idPrefix, '')
}
