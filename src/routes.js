import React from 'react'
import {Route, IndexRoute} from 'react-router'
import JekyllPage from './containers/Views/JekyllPage'
import SelectLanguage from './containers/Views/SelectLanguage'
import NotFound from './containers/Views/NotFound'
import pages from './pages'
import memo from 'memoizee'


const getMdFilesRoutes = memo(function () {
  return Object.keys(pages).reduce((routes, key) => {
    const
      pageRecord = pages[key],
      pageRoutes = Object.keys(pageRecord).map(locale=> {
        const
          page = pageRecord[locale],
          {url} = page

        return (
          <Route
            page={page}
            path={url}
            permalink
            component={JekyllPage}
            key={url}/>
        )
      })

    pageRoutes.forEach(route=>routes.push(route))

    return routes
  }, [])
})

export default () => {
  return (
    <Route path="/" component={SelectLanguage}>
      <IndexRoute component={JekyllPage}/>
      {getMdFilesRoutes()}
      <Route path="*" component={NotFound} key={999} status={404}/>
    </Route>
  )
}

