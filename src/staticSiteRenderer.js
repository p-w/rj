import {get as getPagesByUrl} from './pagesByUrl'
import request from 'request'
import fs from 'fs'
import fse from 'fs-extra'
import path from 'path'
import mkdirp from 'mkdirp'
import i from 'images'
import {getLocaleWatermarkPath} from '../jekyll/_config'

const outputFolder = `${__dirname}/../jekyll/_site`

function savePage(page) {
  const
    urlParts = page.url.split('/'),
    localizedUrl = urlParts.map(part=>encodeURI(part)).join('/'),
    url = `http://localhost:3000${localizedUrl}`
  request(url, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      const
        folderPart = `${outputFolder}${page.url}`,
        fileName = `${folderPart}index.html`,
        destFolder = path.dirname(fileName)

      mkdirp(destFolder, function () {
        fs.writeFile(fileName, body, 'utf8', function (err) {
        })

        const {images} = page
        if (images) {
          Object.keys(images).forEach(key=> {
            const
              pathPart = images[key],
              from = `${__dirname}/../jekyll/${pathPart}`,
              to = `${folderPart}${key}`,
              itemImage = i(from),
              localeWatermarkPath = getLocaleWatermarkPath(page),
              localeWatermark = i(localeWatermarkPath)

            itemImage
              .draw(localeWatermark, 0, 0)
              .save(to, {quality: 95})
          })
        }
      })
      //console.log('saved:', relPath)

    } else {
      const statusCode = response ? response.statusCode : 0

      if (statusCode !== 500) {
        setTimeout(()=> {
          savePage(page)
        }, 1000)
      } else {
        console.log('failed to save:', page.url, statusCode)
      }
    }
  })
}

function saveCss() {
  const url = 'http://localhost:3001/dist/index.css',
    fileName = `${outputFolder}/index.css`

  request(url, function (error, response, body) {
    fs.writeFile(fileName, body, 'utf8', function (err) {
    })
  })
}

export function start() {
  saveCss()
  fse.removeSync(outputFolder)
  fse.emptyDirSync(outputFolder)
  fse.copySync(`${__dirname}/../jekyll/_assets`, `${__dirname}/../jekyll/_site`)
  const pages = getPagesByUrl()
  savePage({url: '/'})
  Object.keys(pages).forEach((key, i)=> {
    setTimeout(()=> {
      savePage(pages[key])
    }, i * 1)
  })
}