FROM node:4.2.6

RUN npm install -g npm@3

RUN git clone https://bitbucket.org/p-w/rj.git && \
    cd rj && \
    npm install

RUN apt-get update && apt-get install -y openssh-server
RUN mkdir /var/run/sshd
RUN echo 'root:root' | chpasswd
RUN sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

RUN npm install -g pm2

RUN git config --global user.email "root@root.root"

CMD cd rj && \
    git pull && \
    rm -rf node_modules/universal-redux && \
    rm -rf node_modules/react-router && \
    npm install && \
    /usr/sbin/sshd && \
    pm2 start bin/start_jekyll_watcher.js && \
    npm run watch-client
