#!/usr/bin/env node
import universal from 'universal-redux'
import universalConfig from '../config/universal-redux.config.js'
import witc from '../config/webpack-isomorphic-tools-config'
import Express from 'express'
import imageHandler from '../src/imageHandler'
const app = new Express()

app.use(imageHandler)

universal.app(app)

universal.setup(universalConfig, witc)
universal.start()
